
// Appends a new message to the message dialog
function addMessage(msg) {
  var msgs = document.getElementById("messages");
  msgs.innerHTML += `<span class="msg">${msg}</span>`;
  msgs.scrollTop = msgs.scrollHeight - msgs.clientHeight;
}

// Initializes a new websocket connection to the server
function initWS() {
  var ws = new WebSocket('ws://' + window.location.host + window.location.pathname);
  ws.onopen = function() { addMessage('Connected to world server.') };
  ws.onclose = function() { addMessage('Connection to world server terminated.'); }
  ws.onmessage = handleServerMsg;
  return ws
}

// Starts a heartbeat to keep the connection alive
function initHB(ws) {
  setInterval(() => { ws.send('🖤') }, 30000);
}

// Handles an incoming server message
function handleServerMsg(rawMsg) {
  console.log(rawMsg);
  var msg = JSON.parse(rawMsg.data);
  if (msg['type'] == 'msg') { addMessage(msg['content']); }
  if (msg['type'] == 'edit') {
    toggleEditor();
    document.getElementById("editor-text").value = msg['text'];
    document.getElementById("editor-status").innerHTML = msg['id'];
  }
}

// Initializes the command line and editor
function initCmd(ws) {
  var cf = document.getElementById('cmd-form');
  var ci = document.getElementById('cmd-input');
  cf.onsubmit = function(e) {
    e.preventDefault();
    if (!frontendCmd(ci.value)) sendCmd(ws, ci.value);
    ci.value = "";
    ci.focus();
  }
  document.getElementById('save').onclick = function() { saveEdit(ws) };
  document.getElementById('exit').onclick = function() { exitEdit(ws) };
  ci.focus();
}

// Handles a frontend command, returns false if not handled
function frontendCmd(cmd) {
  if (/^\s*clear\s*$/.test(cmd)) {
    document.getElementById("messages").innerHTML = '';
    return true;
  }
  return false
}

// Sends a command to the server
function sendCmd(ws, cmd) {
  var msg = { type: 'cmd', content: cmd };
  ws.send(JSON.stringify(msg));
}

// Opens the editor and closes the terminal
function toggleEditor() {
  document.getElementById("terminal").style['display'] = 'none';
  document.getElementById("editor").style['display'] = 'block';
  document.getElementById("editor-text").focus();
}

// Opens the terminal and closes the editor
function toggleTerminal() {
  document.getElementById("editor").style['display'] = 'none';
  document.getElementById("terminal").style['display'] = 'block';
  document.getElementById("cmd-input").focus();
}

// Saves the program of an edited object
function saveEdit(ws) {
  var editData = document.getElementById("editor-text").value;
  var msg = { type: 'sendEdit', editData: editData };
  ws.send(JSON.stringify(msg));
}

// Exits the editor
function exitEdit(ws) {
  var msg = { type: 'stopEdit' };
  ws.send(JSON.stringify(msg));
  toggleTerminal();
}

