require_relative 'backend/server.rb'
require_relative 'docs/xylem.rb'

# Generate Documentation
xyl = Xylem.new 'docs', 'frontend/docs', '/docs', 'about.md'
xyl.build

# Run Server
run Tartarus
