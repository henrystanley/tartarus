
# Grammar for the In-Game Programming Language

PROG = '' | FNDEF PROG
FNDEF = FNNAME '{' EXPRSEQ '}'
EXPRSEQ = '' | EXPR EXPRSEQ
EXPR = STRING | NUMBER | FN | LIST
LIST = '[' EXPRSEQ ']'
STRING = /'[^']*'/
NUMBER = /-?[0-9]+(\.[0-9]+)?/
FN = /[^0-9\[\]\{\}\'\s\-]+/