
# Backend

The system consists of two main components:

A web server to handle user connections
A world server to handle environment state

The web server is responsible for communicating with the frontend client. This involves parsing requests and commands, sending these to the world server, and finally sending any relevant responses to the frontend.

The world server maintains the state of the environment and calculates interaction between the in-game objects. The map is represented by a directed graph with rooms at the vertices and doors between rooms along edges. Rooms contain objects which the player may interact with as well as change the behaviour of. Objects communicate by sending messages with possible arguments to each other, and store state with instance variables . The entire map is serialized to disk as a JSON object. The world server additionally maintains the position of all players within the map.


# Frontend

TODO


# World API

*Actions*:

    Object Actions: move, remove, clone, new, edit

    Room Actions: new, connect, disconnect, list-objects, list-connections

    World Actions: list-rooms, list-objects

    Player Actions: warp-to-room, look-in-room, enter-door, send-msg-to-object


*Properties*:

    Object Properties: variables, name, id, room, handlers

    Room Properties: id, name, players, objects, doors, world

    World Properties: rooms, players, objects
    Player Properties: id, name, room
