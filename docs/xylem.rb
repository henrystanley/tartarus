#!/usr/bin/ruby

require 'kramdown'
require 'fileutils'
require 'yaml'


#########################
### Utility Functions ###
#########################

# Produces a URL safe version of a string
def sanitize(str)
  str.downcase.gsub(' ', '_').gsub(/[^\d\w_]/, "")
end

# Renders an HTML tag
def make_tag(tag, content, attrs={})
  attrs_str = ""
  attrs.map { |x, y| attrs_str << " #{x}=\"#{y}\""}  
  "<#{tag}#{attrs_str}>#{content}</#{tag}>"
end



#############################
### Site Building Classes ###
#############################

# A DocDir represents a directory containing Pages or other PageDirs
class PageDir
  attr_reader :name, :sanitized_name, :path, :parent
  def initialize(dir, mount_path, parent=nil)
    @name = File.basename(dir)
    @sanitized_name = sanitize @name 
    @parent = parent
    @path = @parent ? @parent.path + [(@sanitized_name)] : []
    @children = []
    Dir.glob(dir + '/*').each do |f|
      if File.directory? f then
        @children.push PageDir.new(f, mount_path, self)
      elsif File.extname(f) == '.md' then
        @children.push Page.new(f, mount_path, self)
      elsif File.extname(f) == '.yaml' then
        @children.push YPage.new(f, mount_path, self)
      end
    end
    @children.sort { |x, y| x.name <=> y.name }
    return self
  end

  # Returns the html for each child's nav menu link
  def menu_items(open_path=[])
    @children.map { |x| x.nav_link(open_path) }.join
  end

  # Returns the html for this PageDir's nav menu link
  def nav_link(open_path=[])
    link = make_tag('span', @name, {'class' => 'nav_menu_link'})
    menu_attrs = { 'class' => 'nav_menu_items' }
    menu_attrs['class'] += ' open' if open_path.first == @sanitized_name    
    menu = make_tag('div', menu_items(open_path[1..-1] || []), menu_attrs)
    make_tag('div', link + menu, {'class' => 'nav_menu'}) 
  end

  # Recursively builds pages for all children
  def build(build_dir, template)
    @children.each { |child| child.build(build_dir, template) }
  end
end

# A Page represents a markdown document
class Page
  attr_reader :name, :sanitized_name
  def initialize(filename, mount_path, parent)
    @name = File.basename(filename, ".md")
    @sanitized_name = sanitize @name
    @parent = parent
    @path = @parent.path + [@sanitized_name]
    @mount_path = mount_path
    @markdown = File.read filename
    @html = Kramdown::Document.new(@markdown).to_html
  end
  
  # Builds the nav menu tag for this page
  def nav_link(open_path=[])
    attrs = { 'class' => 'nav_page', 'href' => "#{([@mount_path]+@path).join('/')}.html" }
    attrs['class'] += ' current_page' if @sanitized_name == open_path.first
    make_tag('a', @name, attrs)
  end

  # Builds html page
  def build(build_dir, template)
    top_dir = @parent
    top_dir = top_dir.parent until top_dir.parent.nil?
    nav = top_dir.menu_items(@path)
    page_html = template.gsub('{{navbar}}', nav).gsub("{{content}}", @html).gsub(/YAML_START{[^}]+}YAML_END/, '')
    page_dir_path = "#{build_dir}/#{@parent.path.join('/')}"
    FileUtils.mkdir_p page_dir_path
    File.open("#{page_dir_path}/#{@sanitized_name}.html", 'w') { |f| f << page_html }
  end
end

# A Page represents a yaml document
class YPage
  attr_reader :name, :sanitized_name
  def initialize(filename, mount_path, parent)
    @name = File.basename(filename, ".yaml")
    @sanitized_name = sanitize @name
    @parent = parent
    @path = @parent.path + [@sanitized_name]
    @mount_path = mount_path
    @yaml = YAML.load(File.read(filename))
  end
  
  # Builds the nav menu tag for this page
  def nav_link(open_path=[])
    attrs = { 'class' => 'nav_page', 'href' => "#{([@mount_path]+@path).join('/')}.html" }
    attrs['class'] += ' current_page' if @sanitized_name == open_path.first
    make_tag('a', @name, attrs)
  end

  # Builds html page
  def build(build_dir, template)
    top_dir = @parent
    top_dir = top_dir.parent until top_dir.parent.nil?
    nav = top_dir.menu_items(@path)
    page_html = template
    @yaml.keys.each { |k| page_html = page_html.gsub "@#{k}", @yaml[k] }
    page_html = page_html.gsub('{{navbar}}', nav).gsub('{{content}}', '').gsub('YAML_START{', '').gsub('}YAML_END', '')
    page_dir_path = "#{build_dir}/#{@parent.path.join('/')}"
    FileUtils.mkdir_p page_dir_path
    File.open("#{page_dir_path}/#{@sanitized_name}.html", 'w') { |f| f << page_html }
  end
end



#############
### Xylem ###
#############


class Xylem

  def initialize inputDir, outputDir, mountPath='', indexMd='index.md', staticDir=nil
    @input_dir = inputDir
    @output_dir = outputDir
    @mount_path = mountPath
    @index_md = indexMd
    @static_dir = staticDir
  end

  def build
    # Builds the page tree of the site
    if not Dir.exist?(@input_dir + "/pages") then
      puts "ERROR: No '/pages' directory in #{@input_dir}"
      exit
    end
    page_tree = PageDir.new(@input_dir + '/pages', @mount_path)

    # Reads site template file
    if not File.exist?(@input_dir + "/template.html") then
      puts "ERROR: No 'template.html' file in #{@input_dir}"
      exit
    end
    template = File.read(@input_dir + "/template.html") 

    # Makes the output directory
    FileUtils.rm_rf @output_dir
    FileUtils.mkdir @output_dir

    # Builds index page
    if not File.exist?(@input_dir + '/' + @index_md) then
      puts "ERROR: No '#{@input_dir + '/' + @index_md}' file in #{@input_dir}"
      exit
    end
    index_page = Page.new(@input_dir + '/' + @index_md, @mount_path, page_tree)

    # Builds html pages
    page_tree.build(@output_dir, template)
    index_page.build(@output_dir, template)

    # Moves site's static files if included
    if @static_dir then
      if Dir.exist?(@input_dir + "/static") then
        puts "ERROR: No '/static' directory in #{@input_dir}"
        exit
      end
      FileUtils.cp_r(@input_dir + '/static', @static_dir)
    end

  end

end




