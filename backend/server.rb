require 'sinatra'
require 'sinatra-websocket'
require 'json'
require_relative 'tworld.rb'
require_relative 'client_handler.rb'

class Tartarus < Sinatra::Base

  ## Initialization

  # Set directory for static files
  set :public_folder, 'frontend/static'

  # Init list of client handlers
  set :handlers, []

  # Init TWorld object
  set :world, TWorld.new
  # If a serialized world file exists load it
  if (File.exist? 'world.json') then
    world_json = ''
    File.open('world.json', 'r:UTF-8') { |f| world_json = f.read }
    set :world, TWorld.from_hash(JSON.parse(world_json))
  end
  
  # Print root group password to STDOUT
  puts "Root group ID: #{settings.world.groups['root']}"


  ## Handlers

  # Index handler
  get '/' do
    
    if !request.websocket? # Serve static page if HTTP request
      File.read 'frontend/index.html'
    else # Otherwise handle Websocket connections
      request.websocket do |ws|
        
        # Init new client handler
        handler = ClientHandler.new ws, settings.world, settings.handlers

        # Add client handler to list of handlers and send welcome message
        ws.onopen do
          settings.handlers << handler
          handler.sendMsg 'Welcome to Tartarus!'
        end

        # Handle incoming websocket message
        ws.onmessage do |msg|
          begin
            handler.handleMsg msg
          rescue StandardError => e
            puts "CAUGHT ERROR:"
            puts e.message
            puts e.backtrace.inspect
          end
        end

        # On websocket close, close client handler
        ws.onclose do
          settings.handlers.delete handler
          handler.close
        end

      end
    end
  end

  # Handler for static documentation pages
  get '/docs/*' do
    File.read('frontend/docs/' + params['splat'].join('/'))
  end

end