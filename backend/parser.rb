require 'parslet'
include Parslet

# Parsing Expression Grammar for the Tartarus Language
class TLangParser < Parslet::Parser
  # Whitespace
  rule(:ws) { match('\s').repeat(1).maybe }
  # Number
  rule(:num) { str('-').maybe.as(:sign) >> match('[0-9]').repeat(1).as(:whole) >> (str('.') >> match('[0-9]').repeat(1).as(:fract)).maybe }
  # A String
  rule(:string) { str("'") >> match("[^']").repeat.as(:str) >> str("'") }
  # A Let binding
  rule(:let) { str('~') >> match('[^0-9\[\]\{\}\'~$/\s]').repeat(1).as(:let) }
  # A Set binding
  rule(:set) { str('$') >> match('[^0-9\[\]\{\}\'~$/\s]').repeat(1).as(:set) }
  # A Function symbol
  rule(:fn) { match('[^0-9\[\]\{\}\'~$/\s]').repeat(1).as(:fn) }
  # An Expression
  rule(:expr) { ws >> (num | string | let | set | fn | list | quote) }
  # A list of expressions
  rule(:list) { str('[') >> expr.repeat.as(:list) >> ws >> str(']') }
  # A Quoted expression
  rule(:quote) { str('/') >> expr.as(:quote) }
  # An Action definition
  rule(:fndef) { ws >> match('[^0-9\[\]\{\}\'~$\s]').repeat(1).as(:fnname) >> ws >> str('{') >> expr.repeat.as(:fnbody) >> ws >> str('}') }
  # A Program
  rule(:prog) { fndef.repeat(0) }
  root :prog
end

# AST Node for a Let binding
TLet = Struct::new :name

# AST Node for a Set binding
TSet = Struct::new :name

# AST Node for a Function symbol
TFn = Struct::new :name

# AST Node for a Program
TProg = Struct::new :prog, :visible

# Replaces Let and Set bindings with equivalent function calls
def desugarBinds exprs
  if exprs.is_a? Array then
    exprs_prime = []
    exprs.each do |x|
      case x
      when TLet
        exprs_prime += [x.name, TFn.new('let')]
      when TSet
        exprs_prime += [x.name, TFn.new('set')]
      when Array
        exprs_prime << desugarBinds(x)
      else
        exprs_prime << x
      end
    end
    return exprs_prime
  end
  return exprs
end

# Processes the output of the PEG into a correct AST
class TLangTransform < Parslet::Transform
  # Integers
  rule(:sign => simple(:sign), :whole => simple(:whole)) { (sign ? sign+whole : whole).to_f }
  # Floating point numbers
  rule(:sign => simple(:sign), :whole => simple(:whole), :fract => simple(:fract)) { n=(whole + '.' + fract); (sign ? sign+n : n).to_f }
  # Strings
  rule(:str => subtree(:str)) { str == [] ? '' : str.str }
  # Let Binding
  rule(:let => simple(:let)) { TLet.new let.to_s }
  # Set Binding
  rule(:set => simple(:set)) { TSet.new set.to_s }
  # Function Symbol
  rule(:fn => simple(:fn)) { TFn.new fn.to_s }
  # List of expressions
  rule(:list => subtree(:list)) { desugarBinds(list) }
  # Quoted expression
  rule(:quote => subtree(:quote)) { [quote] }
  # Action Definition
  rule(:fnname => simple(:name), :fnbody => subtree(:body)) {
    if name.to_s[0] == '_' then
      {name.to_s[1..-1] => TProg.new(desugarBinds(body), false)}
    else
      {name.to_s => TProg.new(desugarBinds(body), true)}
    end
  }
end

# Parses a full TLang program and returns an AST
def parseTLangProg prog
  prog.strip!
  return {} if prog == ''
  begin
    parsed = TLangParser.new.parse prog
    transformed = TLangTransform.new.apply parsed
    return transformed.reduce(:merge)
  rescue Parslet::ParseFailed => failure
    puts "SYNTAX ERROR:"
    puts failure.parse_failure_cause.ascii_tree
    return nil
  end
end

# Parses a list of TLang Expressions and returns an AST
def parseTLangExprs exprs
  exprs.strip!
  return [] if exprs == ''
  begin
    parsed = TLangParser.new.expr.repeat.parse exprs
    return desugarBinds(TLangTransform.new.apply(parsed))
  rescue Parslet::ParseFailed => failure
    puts "SYNTAX ERROR:"
    puts failure.parse_failure_cause.ascii_tree
    return nil
  end
end
