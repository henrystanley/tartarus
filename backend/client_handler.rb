require_relative 'tplayer.rb'
require_relative 'parser.rb'
require 'json'

# Class representing a client connection
class ClientHandler

  attr_accessor :ws, :world, :player

  def initialize ws, world, handlers
    @ws = ws # Websocket connection to client
    @world = world # TWorld
    @handlers = handlers # Other active handlers
    @player = TPlayer.new world.lobby, self # TPlayer
  end

  # Send a message to the frontend client
  def sendMsg msg
    @ws.send({'type' => 'msg', 'content' => msg}.to_json)
  end

  # Send a message to the frontend client to initialize the editing of a given TObject's program
  def sendStartEdit obj
    @ws.send({'type' => 'edit', 'id' => obj.id, 'text' => obj.program}.to_json)
  end

  # Parses and dispatches on a recieved message from the client
  def handleMsg msg
    # Keep-Alive Heartbeat
    return if msg == '🖤'
    parsedMsg = JSON.parse msg
    case parsedMsg['type']
    when 'cmd' # Run the parsed command
      processCmd parsedMsg['content']
    when 'sendEdit' # Change the program of an edited TObject
      sendEdit parsedMsg['editData']
    when 'stopEdit' then # Stop editing a TObject
      stopEdit
    end
  end

  # Removes a player from the world and unlocks any TObjects they're editing
  def close
    @player.room.rm_player @player
    stopEdit
  end

  # Used to get the ID of the specified TObject when multiple objects with the same name exist
  # Re-runs the given command with the ID of the selected object
  def disambiguate name, items, cmd
    sendMsg "There are multiple #{name}s, which one did you mean?:"
    sendMsg items.map { |i| "<a href=\"javascript:SC('#{cmd.gsub('{{ID}}', i.id)}');\" class=\"linelink\">#{i.name} - #{i.id}</a>" }.join("<br>")
  end

  # Dispatch method for commands
  def processCmd cmd
    case cmd
    when /save/
      cmd_save
    when /help/
      cmd_help
    when /set\s*name\s*(.+)/
      cmd_setname $1
    when /add\s*group\s+(\S+)\s+(\S+)/
      cmd_addgroup $1, $2
    when /my\s*groups/
      cmd_mygroups
    when /who\s*am\s*i/
      cmd_whoami
    when /where\s*am\s*i/
      cmd_whereami
    when /new\s*group\s+(\S+)/
      cmd_newgroup $1
    when /set\s*group\s+(\S+)\s+(.+)/
      cmd_setgroup $1, $2
    when /new\s*room\s+(.+)/
      cmd_newroom $1
    when /rm\s*room\s+(.+)/
      cmd_rmroom $1
    when /say\s+(.+)/
      cmd_say $1
    when /make\s+(.+)/ # s
      cmd_make $1
    when /clone\s+(.+)/
      cmd_clone $1
    when /clone_with_id\s+(.+)/
      cmd_clone_with_id $1
    when /rm_with_id\s+(.+)/
      cmd_rm_with_id $1
    when /rm\s+(.+)/
      cmd_rm $1
    when /look(\s*)?all/
      cmd_look_all
    when /look/
      cmd_look
    when /my\s*id/
      cmd_my_id
    when /inspect\s+(the\s+)?(.+)/
      cmd_inspect $2
    when /inspect_with_id\s+(.+)/
      cmd_inspect_with_id $1
    when /info\s+(.+)/
      cmd_info $1
    when /info_with_id\s+(.+)/
      cmd_info_with_id $1
    when /actions\s+(.+)/
      cmd_actions $1
    when /actions_with_id\s+(.+)/
      cmd_actions_with_id $1
    when /teleport\s+(to\s+)?(.+)/
      cmd_teleport $2
    when /list\s*rooms/
      cmd_listrooms
    when /edit\s+(the\s+)?(.+)/
      cmd_edit $2
    when /edit_with_id\s+(.+)/
      cmd_edit_with_id $1
    when /([^!]+)!\s+(.+)/
      puts "SEND '#{$1.strip}' '#{$2}'"
      cmd_send $1.strip, $2
    when /send_with_id\s+([^\s]+)\s+(.+)/
      puts "SEND '#{$1.strip}' '#{$2}'"
      cmd_send_with_id $1, $2
    else
      sendMsg "Command [#{cmd}] not understood."
    end
  end

  # Updates the program of the TObject the player is editing with the given new program
  def sendEdit editData
    if !@player.editing.nil? then
      @player.editing.update_program editData
    end
  end

  # Ends the editing of a TObject by this player and unlocks the TObject
  def stopEdit
    if !@player.editing.nil? then
      @player.editing.unlock
      @player.editing = nil
    end
  end

  # Cmd for saving the TWorld to disk
  # Available to root users only
  def cmd_save
    if @player.groups.include? 'root' then
      File.open('world.json', 'w+:UTF-8') do |f|
        f << @world.to_h.to_json
      end
      sendMsg "Saved World."
    else
      sendMsg "You're not authorized to use that command"
    end
  end

  # Sends back help information to the user
  def cmd_help
    sendMsg "Some useful commands:"
    sendMsg [
      "setname [NAME]",
      "addgroup [GROUPNAME] [GROUPKEY]",
      "mygroups",
      "whoami",
      "whereami",
      "newgroup [GROUPNAME]",
      "setgroup [ID] [GROUPNAME]",
      "newroom [NAME]",
      "rmroom [ID]",
      "say [MSG]",
      "make [NAME]",
      "clone [NAME]",
      "rm [NAME]",
      "look",
      "myid",
      "inspect [NAME]",
      "info [NAME]",
      "teleport [ID]",
      "edit [NAME]",
      "[NAME] ! [EXPRS]"
    ].map { |x| "<span class=\"linelink\">#{x}</span>" }.join('<br>')
  end

  # Sets the name of this user to the given name
  def cmd_setname name
    name = rmAngleBrackets name
    @player.name = name
    sendMsg "Set name to: #{name}"
  end

  # If the given group exists and the provided group-password is correct, add this user to the group
  def cmd_addgroup group, groupId
    if !@world.groups.keys.include? group then
      sendMsg "No such group: #{group}"
    else
      if @world.groups[group] == groupId then
        @player.add_group group
        sendMsg "Added group #{group}"
      else
        sendMsg "Incorrect group secret"
      end
    end  
  end

  # Sends a list of the groups to which this user belongs to the client
  def cmd_mygroups
    sendMsg "You belong to the groups: #{@player.groups.join(', ')}"
  end
  
  # Sends this user's name to the client
  def cmd_whoami
    article = ('aeiou'.chars.include? @player.name[0]) ? 'an' : 'a'
    sendMsg "You are #{article} #{@player.name}"
  end
  
  # Sends the room this user is in to the client
  def cmd_whereami
    article = ('aeiou'.chars.include? @player.name[0]) ? 'an' : 'a'
    sendMsg "You are in #{@player.room.name}"
  end

  # If the given name does not belong to an existing group, creates the group and sends back the password
  def cmd_newgroup groupName
    if @world.groups.keys.include? groupName then
      sendMsg "There is already a group called #{groupName}"
    else
      groupPassword = @world.new_group groupName
      sendMsg "Added group #{groupName} with secret: #{groupPassword}"
    end  
  end
  
  # If authorized to do so by this user, sets the group of the given TObject
  def cmd_setgroup id, group
    group = nil if group == 'none'
    obj = @world.get_by_id id
    if obj then
      if obj.authorized_user?(@player) then
        obj.group = group
        sendMsg "Changed #{id} to group #{group || 'none'}"
      else
        sendMsg "You're not authorized to change this group"
      end
    else
      sendMsg "There is no item with id #{id}"
    end
  end

  # Sets the group of the specified object by name
  def cmd_set_object_group name, group
    if @player.room.authorized_user? @player then 
      objs = @player.room.get_by_name name
      case objs.length
      when 0
        sendMsg "There is no #{name}."
      when 1
        group = nil if group == 'none'
        objs.first.group = group
        sendMsg "Changed #{name} to group #{group}"
      else
        disambiguate name, objs, 'set_object_group_with_id {{ID}} #{group}'
      end
    else
      sendMsg "You're not authorized to remove objects in this room"
    end
  end

  # Creates a new TRoom in this world and sends its ID to the user
  def cmd_newroom name
    room = @world.new_room name
    sendMsg "Created room '#{name}' at #{room.id}"
  end
  
  # Removes the given room if the user is authorized to do so
  def cmd_rmroom id
    room = @world.get_by_id id
    if room && room.is_a?(TRoom) then
      if room.authorized_user @user then
        @world.rm_room room
        sendMsg "Removed room '#{id}'"
      else
        sendMsg "You're not authorized to remove this room"
      end
    else
      sendMsg "There is no room with id #{id}"
    end

    if @player.room.authorized_user? @player then
      obj = @player.room.get_by_id id
      if obj.nil? then
        sendMsg "There is no object with id #{id}"
      else
        obj.room.rm_object obj
        sendMsg "Removed #{id}"
      end
    else
      sendMsg "You're not authorized to remove objects in this room"
    end
  end

  # Broadcasts a new message to other users in the same room as this user
  def cmd_say text
    text = rmAngleBrackets text
    @player.room.players.each do |player|
      player.handler.sendMsg "#{@player.name} said: <i>#{text}</i>"
    end
  end

  # Makes a new object in this user's room if they are authorized to do so
  def cmd_make name
    if @player.room.authorized_user? @player then 
      @player.room.new_object name
      sendMsg "You made a: #{name}"
    else
      sendMsg "You're not authorized to create objects in this room"
    end
  end

  # Clones the given object in this room if the user is authorized to
  def cmd_clone name
    if @player.room.authorized_user? @player then 
      objs = @player.room.get_by_name name
      case objs.length
      when 0
        sendMsg "There is no #{name}."
      when 1
        objs.first.clone
        sendMsg "Cloned #{name}"
      else
        disambiguate name, objs, 'clone_with_id {{ID}}'
      end
    else
      sendMsg "You're not authorized to create objects in this room"
    end
  end

  # Clones the object with the provided id if the user is authorized to
  def cmd_clone_with_id id
    if @player.room.authorized_user? @player then 
      obj = @world.get_by_id id
      if obj.nil? || !obj.is_a?(TObject) then
        sendMsg "There is no objects with id #{id}"
      else
        @player.room.objects << obj.clone
        sendMsg "Cloned #{id}"
      end
    else
      sendMsg "You're not authorized to create objects in this room"
    end
  end

  # Removes this provided object if the user is authorized to
  def cmd_rm_with_id id
    if @player.room.authorized_user? @player then
      obj = @player.room.get_by_id id
      if obj.nil? then
        sendMsg "There is no object with id #{id}"
      else
        obj.room.rm_object obj
        sendMsg "Removed #{id}"
      end
    else
      sendMsg "You're not authorized to remove objects in this room"
    end
  end

  # Removes the provided object (by name) if the user is authorized to
  def cmd_rm name
    if @player.room.authorized_user? @player then 
      objs = @player.room.get_by_name name
      case objs.length
      when 0
        sendMsg "There is no #{name}."
      when 1
        objs.first.room.rm_object objs.first
        sendMsg "Removed #{name}"
      else
        disambiguate name, objs, 'rm_with_id {{ID}}'
      end
    else
      sendMsg "You're not authorized to remove objects in this room"
    end
  end

  # Sends back a list of objects in this room, including hidden objects, if the user is authorized to
  def cmd_look_all
    if @player.room.authorized_user? @player then 
      objs = @player.room.objects
      if !objs.empty? then
        obj_links = objs.map { |o|
          article = ('aeiou'.chars.include? o.name[0]) ? 'an' : 'a'
          name_link = "<a href=\"javascript:SC('inspect_with_id #{o.id}');\">#{article} #{o.name}</a>"
          actions_link = o.actions.empty? ? '' : "<a href=\"javascript:SC('actions_with_id #{o.id}');\" class=\"action\">*</a>"
          name_link + actions_link
        }
        objects_text = obj_links[0..-2].join(', ') + ', and ' + obj_links[-1]
        objects_text = obj_links[0] if obj_links.length == 1
        sendMsg "You see: #{objects_text}"
      else
        sendMsg "You stare into the void, and the void stares back..."
      end
    else
      sendMsg "You're not allowed to see behind the curtain in this room"
    end
  end

  # Send back a list of non-hidden objects in this player's room
  def cmd_look
    objs = @player.room.objects.select { |o| o.name[0] != '_' }
    if !objs.empty? then
      obj_links = objs.map { |o|
        article = ('aeiou'.chars.include? o.name[0]) ? 'an' : 'a'
        name_link = "<a href=\"javascript:SC('inspect_with_id #{o.id}');\">#{article} #{o.name}</a>"
        actions_link = o.actions.empty? ? '' : "<a href=\"javascript:SC('actions_with_id #{o.id}');\" class=\"action\">*</a>"
        name_link + actions_link
      }
      p obj_links
      objects_text = obj_links[0..-2].join(', ') + ', and ' + obj_links[-1]
      objects_text = obj_links[0] if obj_links.length == 1
      sendMsg "You see: #{objects_text}"
    else
      sendMsg "You stare into the void, and the void stares back..."
    end
  end

  # Sends back the id of this user
  def cmd_my_id
    sendMsg "Your id is: #{@player.id}"
  end

  # If the given object (by name) exists in this room, send back its description
  def cmd_inspect name
    objs = @player.room.get_by_name name
    case objs.length
    when 0
      sendMsg "There is no #{name}"
    when 1
      cmd_inspect_with_id objs.first.id
    else
      disambiguate name, objs, 'inspect_with_id {{ID}}'
    end
  end

  # If the given object (by ID) exists in this room, send back its description
  def cmd_inspect_with_id id
    obj = @player.room.get_by_id id
    if obj.nil? then
      sendMsg "There is no object with id #{id}"
    else
      sendMsg (obj.description || "A #{obj.name}")
    end
  end

  # Sends back information about the given object (by name)
  def cmd_info name
    objs = @player.room.get_by_name name
    case objs.length
    when 0
      sendMsg "There is no #{name}"
    when 1
      sendMsg objs.first.info
    else
      disambiguate name, objs, 'info_with_id {{ID}}'
    end
  end

  # Sends back information about the given object (by id)
  def cmd_info_with_id id
    obj = @player.room.get_by_id id
    if obj.nil? then
      sendMsg "There is no object with id #{id}"
    else
      sendMsg obj.info
    end
  end

  # Sends back the actions of the given object (by name)
  def cmd_actions name
    objs = @player.room.get_by_name name
    case objs.length
    when 0
      sendMsg "There is no #{name}"
    when 1
      cmd_actions_with_id objs.first.id
    else
      disambiguate name, objs, 'actions_with_id {{ID}}'
    end
  end

  # Sends back the actions of the given object (by ID)
  def cmd_actions_with_id id
    obj = @player.room.get_by_id id
    if obj.nil? then
      sendMsg "There is no object with id #{id}"
    else
      actions = obj.actions.map { |a| 
        "<a href=\"javascript:SC('send_with_id #{obj.id} #{rmAngleBrackets a}');\" class=\"action\">#{rmAngleBrackets a}</a>"
      }
      sendMsg "The #{obj.name} responds to: #{actions.join(', ')}"
    end
  end

  # Move this player to the provided room or object if it exists
  def cmd_teleport id
    item = @world.get_by_id id
    if item.nil? then
      sendMsg "No room or object has the id #{id}"
    else
      @player.move_to item if item.is_a? TRoom
      @player.move_to item.room if item.is_a? TObject
      sendMsg "Teleported to room #{id}"
    end
  end
  
  # Sends back a list of all the rooms in this world
  # This commands is only available to root users
  def cmd_listrooms
    if @player.groups.include? 'root' then
      sendMsg ([@world.lobby, @world.library] + @world.rooms).collect { |r|
        teleport_link = "<a href=\"javascript:SC('teleport #{r.id}');\" class=\"linelink\">#{r.id}</a>"
        "'#{rmAngleBrackets r.name}' - #{teleport_link}"
      }.join('<br>')
    else
      sendMsg "You're not authorized to use that command"
    end
  end

  # Begins editing the given object (by name)
  def cmd_edit name
    objs = @player.room.get_by_name name
    case objs.length
    when 0
      sendMsg "There is no #{name}"
    when 1
      cmd_edit_with_id objs.first.id
    else
      disambiguate name, objs, 'edit_with_id {{ID}}'
    end
  end
  
  # Begins editing the given object (by ID)
  def cmd_edit_with_id object_id
    obj = @player.room.get_by_id object_id
    if obj.nil? then
      sendMsg "There is no object with id #{object_id}"
    elsif !obj.is_a? TObject then
      sendMsg "#{object_id} is not an object"
    elsif (!@player.room.authorized_user? @player) || (!obj.authorized_user? @player)
      sendMsg "You are not authorized to edit this object"
    elsif obj.locked?
      sendMsg "#{object_id} is currently locked, you cannot edit it"
    else
      obj.lock
      @player.editing = obj
      sendStartEdit obj
    end
  end

  # Sends the provided object (by name) the provided expressions to evaluate
  def cmd_send name, exprs
    objs = @player.room.get_by_name name
    case objs.length
    when 0
      sendMsg "There is no #{name}"
    when 1
      cmd_send_with_id objs.first.id, exprs
    else
      disambiguate name, objs, "send_with_id {{ID}} #{exprs}"
    end
  end

  # Sends the provided object (by ID) the provided expressions to evaluate
  def cmd_send_with_id object_id, exprs
    obj = @player.room.get_by_id object_id
    if obj.nil? then
      sendMsg "There is no object with id #{object_id}"
    elsif !obj.is_a? TObject then
      sendMsg "#{object_id} is not an object"
    elsif obj.locked?
      sendMsg "#{object_id} is currently locked, you cannot interact with it"
    else
      exprs = parseTLangExprs exprs.strip
      obj.eval exprs.reverse, @player.id if exprs
    end
  end

end