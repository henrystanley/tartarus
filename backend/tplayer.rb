require_relative 'utils.rb'

# An In-Game Player
class TPlayer

  attr_accessor :id, :name, :room, :groups, :handler, :editing

  def initialize room, handler
    @id = makeID
    @name = "lonely traveler"
    @room = room
    @room.add_player self
    @handler = handler
    @groups = []
    @editing = nil
  end

  # Equality between players
  def ==(a)
    a.is_a?(TPlayer) && @id == a.id
  end

  # Moves this player to the provided room
  def move_to room
    @room.rm_player self
    @room = room
    @room.add_player self
  end

  # Adds a new group to this users list of groups
  def add_group group
    @groups << group
  end

end
