require 'securerandom' # For UUID generation

# Generates a random id of greek letters
def makeID(segLen=4)
  alphabet = "ΑαΒβΓγΔδΕεΖζΗηΘθΙιΚκΛλΜμΝνΞξΟοΠπΡρΣσΤτΥυΦφΧχΨψΩω".chars
  segLen.times.collect { 4.times.collect { alphabet.sample }.join }.join '/'
end

# Converts an object into a pretty printed string
def pp x
  case x
  when Float
    return x.to_i.to_s if x.to_i == x.to_f
    return x.to_s
  when Array
    return "[#{x.map{ |y| pp y}.join(' ')}]"
  when String
    return "'#{x}'"
  when TFn
    return x.name
  end
  return x.to_s
end

# Remove angle brackets from a string
def rmAngleBrackets str
  return str.gsub('<', '').gsub('>', '')
end