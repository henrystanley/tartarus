require_relative 'utils.rb'
require_relative 'eval.rb'
require_relative 'parser.rb'
require 'timeout.rb'

# An in-game object
class TObject

  attr_accessor :id, :name, :room, :group, :evalCtx, :program, :locked

  def initialize name, room
    @id = makeID
    @name = rmAngleBrackets name
    @room = room
    @group = nil
    @evalCtx = TEvalCtx.new self
    @program = ""
    @locked = false
  end

  # Create a new TObject from its corresponding Hash representation
  def self.from_hash h, room
    obj = TObject.new h['name'], room
    obj.id = h['id']
    obj.group = h['group']
    obj.update_program h['program']
    return obj
  end

  # Return the Hash representation for this TObject
  def to_h
    return {
      'id' => @id,
      'name' => @name,
      'group' => @group,
      'program' => @program
    }
  end

  # Returns a new TObject identical to this one except with a new ID
  def clone
    obj = TObject.new @name, @room
    obj.group = @group
    obj.update_program @program
    return obj
  end

  # Equality operation between TObjects
  def ==(a)
    a.is_a?(TObject) && @id == a.id
  end

  # Moves this TObject to the provided Room
  def move_to room
    @room.rm_object self
    @room = room
    @room.add_object self
  end

  # Sets the owner group of this TObject
  def set_group group
    @group = group
  end
  
  # Checks if a given user is allowed to edit this TObject
  def authorized_user? user
    return true if user.groups.include? 'root'
    return true if @group.nil?
    return user.groups.include? @group
  end
  
  # Returns the computed description for this TObject
  def description
    desc = nil
    if @evalCtx.prog['description'] then
      begin
        Timeout::timeout($MAX_EVAL_TIME) do
          stack, _ = @evalCtx.evalFn 'description', [], []
          desc = stack.pop
          desc = pp desc if !desc.is_a? String
          desc = @evalCtx.formatString desc
        end
      rescue Timeout::Error
      end
    end
    return desc
  end

  # Returns the names of all actions this TObject responds to
  def all_actions
    @evalCtx.prog.keys
  end

  # Returns the names of all visible actions this TObject responds to
  def actions
    @evalCtx.prog.select { |k, v| v.is_a?(TProg) && v.visible }.keys
  end

  # Returns the information string for this TObject
  def info
    desc = description ? "<br>Description: #{description}" : ''
    "Name: #{@name}<br>Id: #{@id}<br>Group: #{@group || 'none'}#{desc}"
  end

  # Locks this TObject for editing
  # This prevents the object from being changed by multiple users at the same time
  def lock
    @locked = true
  end

  # Unlocks this TObject after editing is completed
  def unlock
    @locked = false
  end

  # Returns if this TObject is currently locked
  def locked?
    @locked
  end

  # Sets the program associated with this TObject to the one provided and initializes it
  def update_program program
    @program = program
    parsedProg = parseTLangProg program
    if !parsedProg.nil? then
      @evalCtx = TEvalCtx.new self, parsedProg
      if @evalCtx.prog['init'] then
        begin
          Timeout::timeout($MAX_EVAL_TIME) do
            @evalCtx.evalFn 'init', [], []
          end
        rescue Timeout::Error
        end
      end
    else
      @evalCtx = TEvalCtx.new self
    end
  end

  # Evaluates a list of expressions inside the context of this TObject
  def eval exprs, sender=nil
    Timeout::timeout($MAX_EVAL_TIME) do
      exprs = parseTLangExprs exprs if exprs.is_a? String
      s, lv = @evalCtx.evalExprs exprs, [], [], 0, sender if !exprs.nil?
      return s
    end
  end

end