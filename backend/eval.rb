require_relative 'utils.rb'
require_relative 'parser.rb'

# Maximum depth for nested function calls
$MAX_STACK_DEPTH = 100
# Maximum time an expression can take to evaluate in seconds
$MAX_EVAL_TIME = 2

# An evaluation context
class TEvalCtx

  attr_accessor :prog

  def initialize obj, prog={}
    @obj = obj # The object this context is bound with
    @prog = prog # The program for this context
  end

  # Evaluates the given expression or expressions inside this context
  def evalExprs exprs, stack, localVars=[], depth=0, sender=nil
    # Push a new layer of bindings
    localVars.unshift({})
    # Set caller if applicable
    @prog['caller'] = sender if sender
    # Wrap the given expr in an array if it is singular
    exprs = [exprs] if !exprs.is_a? Array
    # Eval each expression in the series
    exprs.each { |e| stack, localVars = eval(e, stack, localVars, depth, sender) }
    # Pop layer of bindings and return
    localVars.shift
    return stack, localVars
  end

  # Evals a single expression inside this context
  def eval expr, stack, localVars, depth=0, sender=nil
    case expr
    when TFn # If the expression is a function call it
      evalFn expr.name, stack, localVars, depth, sender
    else # Otherwise push the expression to the stack
      stack.push expr
    end
    return stack, localVars
  end

  # Gets a binding from the current stack of bindings
  # Returns nil if no such binding exists
  def getP name, localVars
    localVars.each { |lv| return lv[name] if lv[name] }
    return @prog[name] if @prog[name]
    return nil
  end

  # Cleans a given string for sending to the frontend
  # Also adds formating for action links
  def formatString str
    str = rmAngleBrackets str
    str = str.gsub(/\$\(([^|]*)\|([^\)]*)\)/) do
      "<a href=\"javascript:SC('send_with_id #{@obj.id} #{$2}');\" class=\"action\">#{$1}</a>"
    end
    str = str.gsub(/\$\(([^\)]*)\)/) do
      "<a href=\"javascript:SC('send_with_id #{@obj.id} #{$1}');\" class=\"action\">#{$1}</a>"
    end
    return str
  end

  # Evaluates a function
  def evalFn name, stack, localVars, depth=0, sender=nil
    return stack if depth > $MAX_STACK_DEPTH
    # Dispatch for Built In Functions
    case name
    when '.'; bif_print_stack stack, localVars, sender
    when '#'; bif_stack_size stack, localVars
    when 'say'; bif_say stack, localVars
    when 'tell'; bif_tell stack, localVars
    when 'msg'; bif_msg stack, localVars
    when '+'; bif_add stack, localVars
    when '-'; bif_sub stack, localVars
    when '*'; bif_mul stack, localVars
    when '/'; bif_div stack, localVars
    when 'set'; bif_set stack, localVars
    when 'setp'; bif_set_prog stack, localVars
    when 'let'; bif_let stack, localVars
    when 'letp'; bif_let_prog stack, localVars
    when 'getp'; bif_get_prog stack, localVars
    when 'include'; bif_include stack, localVars
    when 'include-as'; bif_include_as stack, localVars
    when 'visible'; bif_visible stack, localVars
    when 'players'; bif_players stack, localVars
    when 'objects'; bif_objects stack, localVars
    when 'self'; bif_self stack, localVars
    when 'name'; bif_name stack, localVars
    when 'actions'; bif_actions stack, localVars
    when 'all-actions'; bif_all_actions stack, localVars
    when 'by-name'; bif_by_name stack, localVars
    when 'rename'; bif_rename stack, localVars
    when 'make'; bif_make stack, localVars
    when 'clone'; bif_clone stack, localVars
    when 'rm'; bif_rm stack, localVars
    when 'move'; bif_move stack, localVars
    when 'if'; bif_if stack, localVars, depth, sender
    when 'if-else'; bif_if_else stack, localVars, depth, sender
    when 'eval'; bif_eval stack, localVars, depth, sender
    when '!'; bif_send stack, localVars, depth
    when '='; bif_eq stack, localVars
    when '<'; bif_lt stack, localVars
    when ':>'; bif_pop_right stack, localVars
    when ':<'; bif_push_right stack, localVars
    when '<:'; bif_pop_left stack, localVars
    when '>:'; bif_push_left stack, localVars
    else # If a BIF is not found, then tries to get the program bound to this function and evaluate it
      prog = getP name, localVars
      if prog then
          return evalExprs prog.prog, stack, localVars, (depth+1), sender if prog.is_a? TProg
          stack.push prog
          return stack, localVars
      end
      # Otherwise push the function to the stack and return
      stack.push name
      return stack, localVars
    end
  end


  # Built In Functions

  # Prints the contents of the current stack
  def bif_print_stack stack, localVars, sender
    player = @obj.room.get_by_id sender
    player.handler.sendMsg stack.map { |x| formatString(pp(x)) }.join(' ') if player
    return stack, localVars
  end

  # Pushes the current size of the stack to the stack
  def bif_stack_size stack, localVars
    stack.push stack.length
    return stack, localVars
  end

  # Broadcasts a string from the stack to all users in the same room as this object
  def bif_say stack, localVars
    return stack, localVars if stack.length < 1
    text = stack.pop
    text = pp text if !text.is_a? String
    text = formatString text
    @obj.room.players.each do |player|
      player.handler.sendMsg "#{@obj.name} said: <i>#{text}</i>"
    end
    return stack, localVars
  end

  # Sends a string to a specific user as if this object said it to them
  def bif_tell stack, localVars
    return stack, localVars if stack.length < 2
    to_player = stack.pop
    text = stack.pop
    text = pp text if !text.is_a? String
    text = formatString text
    player = @obj.room.get_by_id to_player
    if !player.nil? then
        player.handler.sendMsg "#{@obj.name} said to you: <i>#{text}</i>"
    else
      @obj.room.get_by_name(to_player).each do |x|
        x.handler.sendMsg "#{@obj.name} said to you: <i>#{text}</i>"
      end
    end
    return stack, localVars
  end

  # Sends a raw string to a specific user
  def bif_msg stack, localVars
    return stack, localVars if stack.length < 2
    to_player = stack.pop
    text = stack.pop
    text = pp text if !text.is_a? String
    text = formatString text
    player = @obj.room.get_by_id to_player
    if !player.nil? then
        player.handler.sendMsg "#{text}"
    else
      @obj.room.get_by_name(to_player).each do |x|
        x.handler.sendMsg "#{text}"
      end
    end
    return stack, localVars
  end

  # Adds two numbers on the stack
  def bif_add stack, localVars
    return stack, localVars if stack.length < 2
    b = stack.pop
    a = stack.pop
    if a.is_a?(Array) && !b.is_a?(Array) then
      stack.push (a+[b])
    elsif !a.is_a?(Array) && b.is_a?(Array) then
      stack.push ([a]+b)
    elsif a.is_a?(String) && !b.is_a?(String) then
      stack.push (a+pp(b))
    elsif !a.is_a?(String) && b.is_a?(String) then
      stack.push (pp(a)+b)
    else
      stack.push (a+b)
    end
    return stack, localVars
  end

  # Subtracts two numbers on the stack
  def bif_sub stack, localVars
    return stack, localVars if stack.length < 2
    b = stack.pop
    a = stack.pop
    stack.push (a-b)
    return stack, localVars
  end

  # Multiplies two numbers on the stack
  def bif_mul stack, localVars
    return stack if stack.length < 2
    b = stack.pop
    a = stack.pop
    stack.push (a*b)
    return stack, localVars
  end

  # Divides two numbers on the stack
  def bif_div stack, localVars
    return stack, localVars if stack.length < 2
    b = stack.pop
    a = stack.pop
    stack.push (a/b)
    return stack, localVars
  end

  # Binds a function to a value
  def bif_set stack, localVars
    return stack, localVars if stack.length < 2
    key = stack.pop
    return stack, localVars if !key.is_a? String
    @prog[key] = stack.pop
    return stack, localVars
  end

  # Binds a function to a list of expressions to evaluate
  def bif_set_prog stack, localVars
    return stack, localVars if stack.length < 2
    key = stack.pop
    return stack, localVars if !key.is_a? String
    @prog[key] = TProg.new(stack.pop, false)
    return stack, localVars
  end

  # Binds a local function to a value
  def bif_let stack, localVars
    return stack, localVars if stack.length < 2
    key = stack.pop
    return stack, localVars if !key.is_a? String
    localVars.first[key] = stack.pop
    return stack, localVars
  end

  # Binds a local function to a list of expression to evaluate
  def bif_let_prog stack, localVars
    return stack, localVars if stack.length < 2
    key = stack.pop
    return stack, localVars if !key.is_a? String
    localVars.first[key] = TProg.new(stack.pop, false)
    return stack, localVars
  end

  # Gets the list of expressions bound to a function and pushes it to the stack
  def bif_get_prog stack, localVars
    return stack, localVars if stack.length < 1
    key = stack.pop
    return stack, localVars if !key.is_a? String
    prog = getP key, localVars
    if prog then
      if prog.is_a? TProg then
        stack.push prog.prog
      else
        stack.push [prog]
      end
      return stack, localVars
    end
    stack.push []
    return stack, localVars
  end

  # Imports function bindings from the given library object
  def bif_include stack, localVars
    return stack, localVars if stack.length < 1
    world = @obj.room.world
    target = stack.pop
    return stack, localVars if !target.is_a? String
    obj = world.library.get_by_name(target).first || world.get_by_id(target)
    if !obj.nil?
      @prog = obj.evalCtx.prog.clone.merge @prog
    end
    return stack, localVars
  end

  # Imports functions from the given library object and prefixes them
  def bif_include_as stack, localVars
    return stack, localVars if stack.length < 2
    world = @obj.room.world
    moduleName = stack.pop
    target = stack.pop
    return stack, localVars if !moduleName.is_a?(String) || !target.is_a?(String)
    obj = world.library.get_by_name(target).first || world.get_by_id(target)
    if !obj.nil?
      newActions = obj.evalCtx.prog.clone.transform_keys { |k| "#{moduleName}." + k}
      @prog = newActions.merge @prog
    end
    return stack, localVars
  end

  # Sets the visibility of the specified action
  def bif_visible stack, localVars
    return stack, localVars if stack.length < 2
    visible = stack.pop
    key = stack.pop
    @prog[key].visible = (visible != 0) if @prog[key] && @prog[key].is_a?(TProg)
    return stack, localVars
  end

  # Pushes the IDs of the current players in this object's room to the stack
  def bif_players stack, localVars
    stack.push (@obj.room.players.map { |x| x.id })
    return stack, localVars
  end

  # Pushes the IDs of the current objects in this object's room to the stack
  def bif_objects stack, localVars
    stack.push (@obj.room.objects.map { |x| x.id })
    return stack, localVars
  end

  # Pushes this object's ID to the stack
  def bif_self stack, localVars
    stack.push @obj.id
    return stack, localVars
  end

  # Gets the name associated with the provided ID
  def bif_name stack, localVars
    return stack, localVars if stack.length < 1
    obj = @obj.room.get_by_id stack.pop
    stack.push obj.name if obj
    return stack, localVars
  end

  # Pushes the visible actions of the specified object to the stack
  def bif_actions stack, localVars
    return stack, localVars if stack.length < 1
    obj = @obj.room.get_by_id stack.pop
    stack.push obj.actions if obj
    return stack, localVars
  end

  # Pushes all actions of the specified object to the stack
  def bif_all_actions stack, localVars
    return stack, localVars if stack.length < 1
    obj = @obj.room.get_by_id stack.pop
    stack.push obj.all_actions if obj
    return stack, localVars
  end

  # Pushes all IDs of objects in this room with the provided name to the stack
  def bif_by_name stack, localVars
    return stack, localVars if stack.length < 1
    stack.push @obj.room.get_by_name(stack.pop).map { |o| o.id }
    return stack, localVars
  end

  # Sets the name of the object in this room with the provided ID
  def bif_rename stack, localVars
    return stack, localVars if stack.length < 2
    name = stack.pop
    obj = @obj.room.get_by_id stack.pop
    p obj
    obj.name = name if obj
    return stack, localVars
  end

  # Makes a new object in this room with the provided name
  # Only runs if the caller object has permission to edit in this room
  def bif_make stack, localVars
    return stack, localVars if stack.length < 1
    return stack if @obj.room.group != @obj.group
    name = stack.pop
    name = pp(name) if !name.is_a? String
    obj = @obj.room.new_object name
    stack.push obj.id
    return stack, localVars
  end

  # Clones an object in this room with the provided ID
  # Only runs if the caller object has permission to edit in this room
  def bif_clone stack, localVars
    return stack, localVars if stack.length < 1
    world = @obj.room.world
    return stack if (!@obj.room.group.nil?) && (@obj.room.group != @obj.group)
    target = stack.pop
    obj = world.get_by_id target
    if !obj.nil?
      clone = obj.clone
      stack.push clone.id
      @obj.room.objects << clone
    end
    return stack, localVars
  end

  # Removes an object in this room with the provided ID
  # Only runs if the caller object has permission to edit in this room
  def bif_rm stack, localVars
    return stack, localVars if stack.length < 1
    return stack, localVars if (!@obj.room.group.nil?) && (@obj.room.group != @obj.group)
    obj = @obj.room.get_by_id stack.pop
    @obj.room.rm_object obj if obj
    return stack, localVars
  end

  # Moves a object in this room with the provided name to the provided room ID
  def bif_move stack, localVars
    return stack, localVars if stack.length < 2
    room = @obj.room.world.get_by_id stack.pop
    x = @obj.room.get_by_id stack.pop
    x.move_to room if room && x
    return stack, localVars
  end

  # If higher order function
  def bif_if stack, localVars, depth, sender
    return stack, localVars if stack.length < 2
    return stack, localVars if depth > $MAX_STACK_DEPTH
    quote = stack.pop
    pred = stack.pop != 0
    return evalExprs quote, stack, localVars, (depth+1), sender if pred
    return stack, localVars
  end

  # If-Else higher order function
  def bif_if_else stack, localVars, depth, sender
    return stack, localVars if stack.length < 3
    return stack, localVars if depth > $MAX_STACK_DEPTH
    f_quote = stack.pop
    t_quote = stack.pop
    pred = stack.pop != 0
    return evalExprs t_quote, stack, localVars, (depth+1), sender if pred
    return evalExprs f_quote, stack, localVars, (depth+1), sender
  end

  # Evaluates the provided list of expressions
  def bif_eval stack, localVars, depth, sender
    return stack, localVars if stack.length < 1
    return stack, localVars if depth > $MAX_STACK_DEPTH
    quote = stack.pop
    return evalExprs quote, stack, localVars, (depth+1), sender
  end

  # Calls the provided list of expressions against the specified object in this room
  def bif_send stack, localVars, depth
    return stack, localVars if stack.length < 2
    return stack, localVars if depth > $MAX_STACK_DEPTH
    obj = @obj.room.get_by_id stack.pop
    quote = stack.pop
    if obj then
      begin
        Timeout::timeout($MAX_EVAL_TIME) do 
          s, lv = obj.evalCtx.evalExprs(quote, [], localVars, depth, @obj.id)
          stack.push s
        end
      rescue Timeout::Error
      end
    end
    return stack, localVars
  end

  # Checks if two expression are equal
  def bif_eq stack, localVars
    return stack, localVars if stack.length < 2
    stack.push(stack.pop == stack.pop ? 1 : 0)
    return stack, localVars
  end

  # Compares two numbers
  def bif_lt stack, localVars
    return stack, localVars if stack.length < 2
    stack.push(stack.pop > stack.pop ? 1 : 0)
    return stack, localVars
  end

  # Pops an expression out of the right side of a quote
  def bif_pop_right stack, localVars
    return stack, localVars if stack.length < 1
    return stack, localVars if stack.last == [] || stack.last == ''
    obj = stack.pop
    if obj.is_a?(Array) || obj.is_a?(String) then
      stack.push obj[0..-2]
      stack.push obj[-1]
    end
    return stack, localVars
  end

  # Pushes an expression into the right side of a quote
  def bif_push_right stack, localVars
    return stack, localVars if stack.length < 2
    x = stack.pop
    quote = stack.pop
    stack.push quote + [x] if quote.is_a? Array
    return stack, localVars
  end

  # Pops an expression out of the left side of a quote
  def bif_pop_left stack, localVars
    return stack, localVars if stack.length < 1
    return stack, localVars if stack.last == [] || stack.last == ''
    obj = stack.pop
    if obj.is_a?(Array) || obj.is_a?(String) then
      stack.push obj[0]
      stack.push obj[1..-1]
    end
    return stack, localVars
  end

  # Pushes an expression into the left side of a quote
  def bif_push_left stack, localVars
    return stack, localVars if stack.length < 2
    quote = stack.pop
    x = stack.pop
    stack.push [x] + quote if quote.is_a? Array
    return stack, localVars
  end

end