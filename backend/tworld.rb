require_relative 'utils.rb'
require_relative 'troom.rb'

# A world containing a number of rooms
class TWorld

  attr_accessor :lobby, :library, :rooms, :groups

  def initialize
    # Every world has a lobby, which serves as a default room
    @lobby = TRoom.new 'lobby', self
    @lobby.set_group 'root'

    # Every world also has a library, which serves as a place to store object templates
    @library = TRoom.new 'library', self
    @library.set_group 'root'

    # Init empty list of rooms
    @rooms = []

    # Every world has the root group, which encompasses all essential rooms
    @groups = {'root' => makeID(2)}
  end

  # Returns a new TWorld from its provided Hash representation
  def self.from_hash h
    world = TWorld.new
    world.lobby = TRoom.from_hash h['lobby'], world if h['lobby']
    world.library = TRoom.from_hash h['library'], world if h['library']
    world.rooms = h['rooms'].map { |x| TRoom.from_hash x, world }
    world.groups = h['groups']
    return world
  end

  # Converts world to hash
  def to_h
    return {
      'lobby' => @lobby.to_h,
      'library' => @library.to_h,
      'rooms' => @rooms.map { |x| x.to_h },
      'groups' => @groups
    }
  end

  # Gets a room or object from a provided id
  # Returns nil if id is not found
  def get_by_id id
    return @lobby if @lobby.id == id
    return @library if @library.id == id
    obj = @lobby.get_by_id id
    return obj if obj
    @rooms.each do |room|
      return room if room.id == id
      obj = room.get_by_id id
      return obj if obj
    end
    return nil
  end

  # Creates a new room and returns it
  def new_room name
    room = TRoom.new name, self
    @rooms << room
    return room
  end

  # Given the room deletes it
  # If players are inside, moves them to the lobby
  def rm_room room
    room.players.each { |player| player.move_to @lobby }
    @rooms.delete room
  end

  # Creates a new group and returns its password
  def new_group groupName
    groupId = makeID 2
    groupName = rmAngleBrackets groupName
    @groups[groupName] = groupId
    return groupId
  end

end





