require_relative 'utils.rb'
require_relative 'tobject.rb'

# An in-game room containing both objects and players
class TRoom

  attr_accessor :id, :name, :world, :objects, :players, :group

  def initialize name, world
    @id = makeID
    @name = rmAngleBrackets name
    @world = world
    @objects = []
    @players = []
    @group = nil
  end

  # Returns a new TRoom from its given Hash representation
  def self.from_hash h, world
    room = TRoom.new h['name'], world
    room.id = h['id']
    room.objects = h['objects'].map { |x| TObject.from_hash x, room }
    room.group = h['group']
    return room
  end

  # Returns the Hash representation of this TRoom
  def to_h
    return {
      'id' => @id,
      'name' => @name,
      'objects' => @objects.map { |x| x.to_h },
      'group' => @group
    }
  end

  # Equality operation for TRooms
  def ==(a)
    a.is_a? TRoom && @id == a.id
  end

  # Adds the provided TPlayer to this TRoom
  def add_player player
    @players << player
  end

  # Adds the provided TObject to this TRoom
  def add_object object
    @objects << object
  end

  # Removes the given TPlayer from this TRoom
  def rm_player player
    @players.delete player
  end

  # Initializes a new TObject in this TRoom and returns it
  def new_object name
    obj = TObject.new name, self
    @objects << obj
    return obj
  end

  # Removes the provided TObject from this TRoom
  def rm_object obj
    @objects.delete obj
  end

  # Gets all TObjects in this TRoom with the provided name
  def get_by_name name
    @objects.select { |o| o.name == name }
  end

  # Gets the TPlayer or TObject in this TRoom with the provided ID
  # Returns nil if no such player or object can be found
  def get_by_id id
    @players.each { |x| return x if x.id == id }
    @objects.each { |x| return x if x.id == id }
    return nil
  end

  # Sets the group of this TRoom
  def set_group group
    @group = group
  end

  # Checks if a given TPlayer is authorized to edit things in this TRoom
  def authorized_user? user
    return true if user.groups.include? 'root'
    return true if @group.nil?
    return user.groups.include? @group
  end

end