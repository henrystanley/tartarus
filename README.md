# Tartarus

[DEMO VIDEO](https://player.vimeo.com/video/385456130)

## Concept

Tartarus is a web based MUD like environment. Users connect to a persistent world of interactive programmable objects via a webapp frontend. Interaction with the environment is mediated through a text based interface.

The world takes on the role of a virtual gallery, where each room contains various objects and characters which function as art pieces or tell stories. Further, the users are empowered with the ability to create their own objects and change the behaviour of any existing object, enabling them to create their own works.

Like works of interactive fiction, exploration and interaction within the world are based around text which informs the player about their surroundings and the possible actions they may perform. Building upon this, due to the dynamic programmable nature of the environment far more complex narrative systems can be defined than are possible with traditional graph based interactive fiction tools.


## How to Run Tartarus

To start the Tartarus Server clone this repo and get all the needed dependencies (you'll need the Bundler gem):

    $ git clone https://gitlab.com/henrystanley/tartarus.git
    $ cd tartarus && bundler install

Then just run `rackup`, the server will start on port 9292 by default:

    $ rackup


## Similar Projects

[Paradise](https://github.com/hundredrabbits/Paradise)

[TinyMUCK](https://en.wikipedia.org/wiki/TinyMUCK)

[Else Heart.Break()](http://elseheartbreak.com/)


